<?php
namespace Admin\Controller;
use Think\Controller;
class GoodsController extends Controller {

    // 显示和处理表单
    public function add(){

        $this->assign(
            array(
                '_page_title'=>'添加新商品',
                '_page_btn_name'=>'商品列表',
                '_page_btn_link'=>U('Goods/lst'),
            )
        );

        // 判断用户是否提交了表单
        if(IS_POST){
            $model = D('goods');
            // 2.CREATE方法 a.接收数据并保存到模型中 b.根据模型中定义的规则验证表单
            /**
             * 第一个参数：要接收的数据默认是$_POST
             * 第二个参数：表单的类型。当前是添加还是修改的表单，1：添加 2:修改
             * $_POST:表单中原始的数据 I('post.'):过滤之后的$_POST的数据，过滤XSS攻击
             */

            if($model->create(I('post.'),1)){
                // 插入到数据库中
                if($model->add()){   // 在add()里又先调用了_before_insert方法
                    // 显示成功信息并等待1秒之后跳转
                    $this->success('操作成功！',U('lst'),3); // 3秒之后跳转，默认1秒
                    exit;
                }
            }
            // 如果走到这里，说明上面失败了，在这里处理失败的请求
            $error = $model->getError();
            // 由控制器显示错误信息，并在3秒后跳回上一个页面
            $this->error($error); // 默认就是当前页
        }
        $this->display();
    }


    public function edit(){

        $this->assign(
            array(
                '_page_title'=>'修改商品信息',
                '_page_btn_name'=>'商品列表',
                '_page_btn_link'=>U('Goods/lst'),
            )
        );


        $id = I('get.id');  // 要修改商品的ID
        $model = D('goods');

        // 判断用户是否提交了表单
        if(IS_POST){
            $model = D('goods');
            if($model->create(I('post.'),2)){
                // 插入到数据库中
                if(false !== $model->save()){   // 在save()的返回值是 如果失败返回false,如果成功返回受影响的条数【如果修改前和修改后相同就会返回0】
                    // 显示成功信息并等待1秒之后跳转
                    $this->success('操作成功！',U('lst'),3); // 3秒之后跳转，默认1秒
                    exit;
                }
            }
            // 如果走到这里，说明上面失败了，在这里处理失败的请求
            $error = $model->getError();
            // 由控制器显示错误信息，并在3秒后跳回上一个页面
            $this->error($error); // 默认就是当前页
        }
        // 根据id取出要修改的商品原信息
        $data = $model->find($id);
        $this->assign('data',$data);

        $this->display();
    }


    public function delete(){
        $model = D('goods');
        if(false !== $model->delete(I('get.id')))
            $this->success('删除成功！',U('lst'));
        else
            $this->error('删除失败!原因：'.$model->getError());
    }



    public function lst(){

        $this->assign(
            array(
                '_page_title'=>'商品列表',
                '_page_btn_name'=>'添加新商品',
                '_page_btn_link'=>U('Goods/add'),
            )
        );

        $model = D('goods');
        
        // 返回数据和翻页
        $data = $model->search();

        // 以下三种assign效果一样：
        // 第一种
        $this->assign($data);
        // 第二种
        $this->assign('data',$data['data']);
        $this->assign('page',$data['page']);
        // 第三种
        $this->assign(array(
            'data' => $data['data'],
            'page' => $data['page'],
        ));

        $this->display();
    }
}