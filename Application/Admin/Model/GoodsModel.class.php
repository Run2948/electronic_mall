<?php
/**
 * Created by PhpStorm.
 * User: Qing
 * Date: 2018-03-10
 * Time: 20:57
 */

namespace Admin\Model;

use Think\Model;

class GoodsModel extends Model
{
    // 添加时调用create方法允许接收的字段
    protected $insertFields = 'goods_name,market_price,shop_price,is_on_sale,goods_desc';

    // 修改时调用create方法允许接收的字段
    protected $updateFields = 'id,goods_name,market_price,shop_price,is_on_sale,goods_desc';

    // 定义验证规则
    protected $_validate = array(
        array('goods_name', 'require', '商品名称不能为空', 1), // 1:代表必须验证
        array('market_price', 'currency', '市场价格必须是货币类型', 1),
        array('shop_price', 'currency', '市场价格必须是货币类型', 1)
    );

    // 这个方法在添加之前会自动被调用 -->钩子函数
    // 第一个参数：表单中即将要插入到数据库中的数据 -> 数组
    // & 按引用传递：函数内部要修改函数外部传递进来的变量，必须按引用传递，除非传递的是一个对象，因为对象默认是按引用传递的
    protected function _before_insert(&$data, $option)
    {
        //die(removeXSS($_POST['goods_desc']));  // 测试返回值

        /*********************  处理LOGO ***********************/
        // 1.判断有没有图片
//        if ($_FILES['logo']['error'] == 0) {
//            $upload = new \Think\Upload();// 实例化上传类
//            $upload->maxSize = 1024 * 1024;// 设置附件上传大小 1M
//            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
//            $upload->rootPath = './Public/Uploads/'; // 设置附件上传根目录
//            $upload->savePath = 'Goods/'; // 设置附件上传（子）目录  二级目录
//            // 上传文件
//            $info = $upload->upload();
//
//            if (!$info) {
//                // 获取失败原因,把错误信息保存在 模型中的error属性中，然后在控制器里会调用$model->getError()获取到错误信息并由控制台打印
//                $this->error = $upload->getError();
//                return false;
//            } else {// 上传成功 生成缩略图
//                //$this->success('上传成功！');
//
//                /***************************  生成缩略图 **************************/
//                // 先拼上原图上的路径
//                $logo = $info['logo']['savepath'] . $info['logo']['savename'];
//
//                $sm_logo = $info['logo']['savepath'] . 'sm_logo' . $info['logo']['savename'];
//                $mid_logo = $info['logo']['savepath'] . 'mid_logo' . $info['logo']['savename'];
//                $big_logo = $info['logo']['savepath'] . 'big_logo' . $info['logo']['savename'];
//                $mbig_logo = $info['logo']['savepath'] . 'mbig_logo' . $info['logo']['savename'];
//
//                $image = new \Think\Image();
//                // 打开要生成的缩略图的图片
//                $image->open('./Public/Uploads/' . $logo);
//                // 生成缩略图
//                $image->thumb(700, 700)->save('./Public/Uploads/' . $big_logo);
//                $image->thumb(350, 350)->save('./Public/Uploads/' . $mbig_logo);
//                $image->thumb(130, 130)->save('./Public/Uploads/' . $mid_logo);
//                $image->thumb(50, 50)->save('./Public/Uploads/' . $sm_logo);
//
//                /*********************** 把路径存放到表单中 ************************/
//                $data['logo'] = $logo;
//                $data['sm_logo'] = $sm_logo;
//                $data['mid_logo'] = $mid_logo;
//                $data['big_logo'] = $big_logo;
//                $data['mbig_logo'] = $mbig_logo;
//
//                //var_dump($info); die;
//            }
//        }
        if ($_FILES['logo']['error'] == 0) {
            $ret = uploadOne('logo', 'Goods', array(
                array(700, 700),
                array(350, 350),
                array(130, 130),
                array(50, 50),
            ));

            $data['logo'] = $ret['images'][0];
            $data['mbig_logo'] = $ret['images'][1];
            $data['big_logo'] = $ret['images'][2];
            $data['mid_logo'] = $ret['images'][3];
            $data['sm_logo'] = $ret['images'][4];
        }
        // 获取当前时间并添加到表单中
        $data['addtime'] = date('Y-m-d H:i:s', time());
        // 我们自己来过滤这个字段
        $data['goods_desc'] = removeXSS($_POST['goods_desc']);
    }

    /** 扩展其他钩子方法：
     * _before_insert(&$data,$option)
     * _after_insert(&$data,$option)
     * _before_update(&$data,$option)
     * _after_update(&$data,$option)
     * _before_delete($option)
     * _after_delete($option)
     */


    protected function _before_update(&$data, $option)
    {
        $id = $option['where']['id']; //I('get.id');  // 要修改商品的ID
        /*********************  处理LOGO ***********************/
        // 1.判断有没有图片
        if ($_FILES['logo']['error'] == 0) {
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize = 1024 * 1024;// 设置附件上传大小 1M
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath = './Public/Uploads/'; // 设置附件上传根目录
            $upload->savePath = 'Goods/'; // 设置附件上传（子）目录  二级目录
            // 上传文件
            $info = $upload->upload();

            if (!$info) {
                // 获取失败原因,把错误信息保存在 模型中的error属性中，然后在控制器里会调用$model->getError()获取到错误信息并由控制台打印
                $this->error = $upload->getError();
                return false;
            } else {// 上传成功 生成缩略图
                //$this->success('上传成功！');

                /***************************  生成缩略图 **************************/
                // 先拼上原图上的路径
                $logo = $info['logo']['savepath'] . $info['logo']['savename'];

                $sm_logo = $info['logo']['savepath'] . 'sm_logo' . $info['logo']['savename'];
                $mid_logo = $info['logo']['savepath'] . 'mid_logo' . $info['logo']['savename'];
                $big_logo = $info['logo']['savepath'] . 'big_logo' . $info['logo']['savename'];
                $mbig_logo = $info['logo']['savepath'] . 'mbig_logo' . $info['logo']['savename'];

                $image = new \Think\Image();
                // 打开要生成的缩略图的图片
                $image->open('./Public/Uploads/' . $logo);
                // 生成缩略图
                $image->thumb(700, 700)->save('./Public/Uploads/' . $big_logo);
                $image->thumb(350, 350)->save('./Public/Uploads/' . $mbig_logo);
                $image->thumb(130, 130)->save('./Public/Uploads/' . $mid_logo);
                $image->thumb(50, 50)->save('./Public/Uploads/' . $sm_logo);

                /*********************** 把路径存放到表单中 ************************/
                $data['logo'] = $logo;
                $data['sm_logo'] = $sm_logo;
                $data['mid_logo'] = $mid_logo;
                $data['big_logo'] = $big_logo;
                $data['mbig_logo'] = $mbig_logo;

                /****************  删除原图片  ****************/
                // 先查询出原来图片的路径
                $oldLogo = $this->field('logo,mbig_logo,big_logo,mid_logo,sm_logo')->find($id);
                // 从硬盘上删掉  -- PHP中没有批量删除
//                unlink('./Public/Uploads/' . $oldLogo['logo']);
//                unlink('./Public/Uploads/' . $oldLogo['sm_logo']);
//                unlink('./Public/Uploads/' . $oldLogo['mid_logo']);
//                unlink('./Public/Uploads/' . $oldLogo['mbig_logo']);
//                unlink('./Public/Uploads/' . $oldLogo['big_logo']);
                deleteImage($oldLogo);
            }
        }
        // 我们自己来过滤这个字段
        $data['goods_desc'] = removeXSS($_POST['goods_desc']);
    }


    protected function _before_delete($option)
    {
        $id = $option['where']['id']; //I('get.id');  // 要删除商品的ID
        /****************  删除原图片  ****************/
        // 先查询出原来图片的路径
        $oldLogo = $this->field('logo,mbig_logo,big_logo,mid_logo,sm_logo')->find($id);
        // 从硬盘上删掉  -- PHP中没有批量删除
//        unlink('./Public/Uploads/' . $oldLogo['logo']);
//        unlink('./Public/Uploads/' . $oldLogo['sm_logo']);
//        unlink('./Public/Uploads/' . $oldLogo['mid_logo']);
//        unlink('./Public/Uploads/' . $oldLogo['mbig_logo']);
//        unlink('./Public/Uploads/' . $oldLogo['big_logo']);
        deleteImage($oldLogo);
    }


    /**
     * @param int $perPage
     * @return array
     */
    public function search($perPage = 15)
    {

        /*****************  搜索  ******************/
        $where = array(); // 空的where条件
        // 商品名称
        $gn = I('get.gn');
        if ($gn)
            $where['goods_name'] = array('like', "%$gn%");

        // 本店价格
        $fp = I('get.fp');
        $tp = I('get.tp');
        if ($fp && $tp)
            $where['shop_price'] = array('between', array($fp, $tp)); // where shop_price between $fp and $tp
        elseif ($fp)
            $where['shop_price'] = array('egt', $fp); // where shop_price >= $fp
        elseif ($tp)
            $where['shop_price'] = array('elt', $tp); // where shop_price <= $tp

        // 是否上架
        $ios = I('get.ios');
        if ($ios)
            $where['is_on_sale'] = array('eq', $ios);

        // 添加时间
        $fa = I('get.fa');
        $ta = I('get.ta');
        if ($fp && $tp)
            $where['addtime'] = array('between', array($fp, $tp)); // where addtime between $fa and $ta
        elseif ($fp)
            $where['addtime'] = array('egt', $fa); // where addtime >= $fa
        elseif ($tp)
            $where['addtime'] = array('elt', $ta); // where addtime <= $ta

        /*****************  排序  ******************/
        $orderby = 'id';
        $orderway = 'desc';

        $odby = I('get.odby');
        if ($odby) {
            if ($odby == 'is_asc')
                $orderway = 'asc';
            elseif ($odby == 'price_desc')
                $orderby = 'shop_price';
            elseif ($odby == 'price_asc') {
                $orderby = 'shop_price';
                $orderway = 'asc';
            }
        }


        /*****************  翻页  ******************/
        // 取出总的记录数
        $count = $this->where($where)->count();
        // 生成翻页类的对象
        $pageObj = new \Think\Page($count, $perPage);
        // 设置样式
        $pageObj->setConfig('next', '下一页');
        $pageObj->setConfig('prev', '上一页');

        // 生成页面下面显示的上一页、下一页的字符串
        $pageString = $pageObj->show();

        /*****************  取某一页的数据 ******************/
        $data = $this->order("$orderby $orderway")->where($where)->limit($pageObj->firstRow . ',' . $pageObj->listRows)->select();

        /*****************  返回数据 ******************/
        return array(
            'data' => $data, // 数据
            'page' => $pageString, // 翻页字符串
        );
    }


}