<?php
/**
 * Created by PhpStorm.
 * User: Qing
 * Date: 2018-03-12
 * Time: 8:50
 */

// 函数内部要修改函数外部传递进来的变量，必须按引用传递，除非传递的是一个对象，因为对象默认是按引用传递的

$data = array(123);

function bb(&$data){
    $data[] = '234';
}

bb($data);

var_dump($data);

?>