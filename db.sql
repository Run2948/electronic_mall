-- 推荐使用InnoDB,因为它有很好的故障恢复功能。
-- MyISAM: 只有插入和查询操作、性能更快、故障回复能力比较差，容易丢失数据
create database php39;
use php39;
set names utf8;

drop table if exists p39_goods;
create table p39_goods(
  id mediumint unsigned not null auto_increment comment 'Id',
  goods_name varchar(150) not null comment '商品名称',
  market_price decimal(10,2) not null comment '市场价格',
  shop_price decimal(10,2) not null comment '本店价格',
  goods_desc longtext not null comment '商品描述',
  is_on_sale enum('是','否') not null default '是' comment '是否上架',
  is_delete enum('是','否') not null default '否' comment '是否放到回收站',
  addtime datetime not null comment '添加时间',
  primary key (id),
  key shop_price(shop_price),
  key addtime(addtime),
  key is_on_sale(is_on_sale)
)engine=InnoDB default charset=utf8 comment '商品';

-- select * from p39_goods where goods_name like '%xxx%'
-- 说明：大文本字段需要使用全文索引，但是MySQL中全文索引不支持中文
-- 我们以后会学习SPHINX【全文索引引擎】来优化查询数据


-- 修改商品表结构 --> 添加商品图片
use php39;
set names utf8;
alter table p39_goods add logo varchar(150) not null default '' comment '原图';
alter table p39_goods add sm_logo varchar(150) not null default '' comment '小图';
alter table p39_goods add mid_logo varchar(150) not null default '' comment '中图';
alter table p39_goods add big_logo varchar(150) not null default '' comment '大图';
alter table p39_goods add mbig_logo varchar(150) not null default '' comment '更大图';








